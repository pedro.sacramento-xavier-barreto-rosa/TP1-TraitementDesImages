% génération d'une nouvelle vidéo
% où chaque image subit la transformation
% précédemment étudiée, puis une rotation
% autour de l'axe optique
function videorec(nom_ent,nom_sor)
if nargin==0
    nom_ent='videosource.mp4';
    nom_sor='videotransf.mp4';
end
% calcul de la transformation pour
% que le pôle céleste soit au centre
% de l'image
tform=calctform();
% vitesse de rotation de la Terre (en rd/s)
vitesse_rotation=7.292115e-5;
ratio_t=650;
t_deb=8;t_fin=83;
% centre de la rotation appliquée
centre=[640 360];
v_ent = VideoReader(nom_ent);
v_sor=VideoWriter(nom_sor,'MPEG-4');
v_sor.FrameRate=v_ent.FrameRate;
open(v_sor)
timecode=t_deb;
v_ent.CurrentTime=t_deb;
figure
while timecode<t_fin
    timecode=v_ent.CurrentTime;
    temps=ratio_t*(timecode-t_deb);
    angle_rotation=temps*vitesse_rotation;
    Rot=[cos(angle_rotation) -sin(angle_rotation);...
        sin(angle_rotation) cos(angle_rotation)];
    Tra=[centre(1) centre(2)];
    Tra=Tra-Tra*Rot;
    A=[Rot zeros(2,1);...
    Tra 1];
    T=affine2d(A);
    fprintf(1,'%f\n',timecode);
    Frame=readFrame(v_ent);
    R=imref2d(size(Frame));
    Frame = imwarp(Frame,tform,'OutputView',imref2d(size(Frame)));
    Frame=imwarp(Frame,T,'cubic','OutputView',R);
    image(Frame)
    axis equal
    drawnow
    writeVideo(v_sor,Frame)
end
close(v_sor)