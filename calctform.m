function tform=calctform
% Calcul des paramètres de la transformation
[Rot,alph,u0,v0]=calibration;

%%%%%%%%%%%%%A MOFIFIER%%%%%%%%%%%%%
% Matrice intervenant dans le passage 2D-->3D
Mat=[0.005        0   -1;...
         0    0.005   -1;...
         0        0    1];

%%%%%%%%%%%%%A MOFIFIER%%%%%%%%%%%%%
% Nouveaux paramètres pour la reprojection
alphp=200;u0p=u0;v0p=v0;



PrjSor=[alphp 0 u0p;0 alphp v0p;0 0 1];



% Transformée finale
Trf=PrjSor*Rot'*Mat;
% Construction d'un "objet" Matlab correspondant
% à cette matrice (la transposition n'est là que
% pour assurer la bonne convention avec la toolbox Image Processing)
tform = projective2d(Trf');