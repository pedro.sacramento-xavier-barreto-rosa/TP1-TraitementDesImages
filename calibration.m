% Calcul des paramètres de la transformation
% Rot : matrice de rotation
% alph: focale exprimée en pixels
% u0,v0 : coordonnées du point correspondant à l'axe optique
function [Rot,alph,u0,v0]=calibration

%%%%%%%%%%%%%A MOFIFIER%%%%%%%%%%%%%
% On suppose que le centre de l'image correspond bien à l'axe optique du
% capteur.
u0=500;v0=500;

%%%%%%%%%%%%%A MOFIFIER%%%%%%%%%%%%%
% les positions des étoiles dans l'image sont dans uv0
% (format [colonne; ligne], stockées colonne par colonne)
uv0=[0 0;1000 0;0 1000;1000 1000]';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%A MOFIFIER%%%%%%%%%%%%%
% les directions des étoiles sont dans XYZ0
% (format [X0; Y0; Z0], stockées colonne par colonne)
XYZ0=[-1 -1 2;1 -1 2;-1 1 2;1 1 2]';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('uv0')
disp(uv0)
disp('XYZ0')
disp(XYZ0)

% valeurs initiales pour les paramètres
param0=[0 0 0 0];
% lancement de l'optimisation
param=lsqnonlin(@(param) recherche(param,XYZ0,uv0,u0,v0),param0,[],[],...
    optimoptions('lsqnonlin','Display','off'));

% après convergence, affichage des paramètres obtenus
L=param(1);T=param(2);R=param(3);alph=param(4);
disp('L(en °),T(en °),R(en °),alpha(en pixels)')
disp(param)
% validation de la solution en affichant les écarts
uv=projection_infini(XYZ0,u0,v0,alph,L,T,R);
disp('uv-uv0 (en pixels)')
disp(uv-uv0)
disp('écart type')
disp(std(uv-uv0,0,'all'))

% matrice de rotation obtenue
Rot=matrice_rotation(L,T,R);

% fonction appelée autant de fois que nécessaire
% par la fonction lsqnonlin
% param est constitué des 3 angles L,T,R et alpha
% (on minimise sum((uv-uv0).^2),'all') )
function erreur=recherche(param,XYZ0,uv0,u0,v0)
L=param(1);T=param(2);R=param(3);alph=param(4);
uv=projection_infini(XYZ0,u0,v0,alph,L,T,R);
erreur=uv-uv0;

% calcul des projections
% XYZ peut être un vecteur colonne 3x1 ou
% une matrice 3xk pour traiter k points
% uv sera alors un vecteur colonne 2x1 ou
% une matrice 2xk
function uv=projection_infini(XYZ,u0,v0,alph,L,T,R)
Rot=matrice_rotation(L,T,R);
Prj=[alph 0 u0;0 alph v0;0 0 1];
uv=Prj*Rot*XYZ;
uv=uv./uv(3,:);
uv=uv(1:2,:);

% matrice "RTL" de rotation
function Rot=matrice_rotation(lacet,tangage,roulis)
L=[cosd(lacet) 0 -sind(lacet);0 1 0;sind(lacet) 0 cosd(lacet)];
T=[1 0 0; 0 cosd(tangage) sind(tangage);0 -sind(tangage) cosd(tangage)];
R=[cosd(roulis) sind(roulis) 0;-sind(roulis) cosd(roulis) 0;0 0 1];
Rot=R*T*L;