import numpy as np
import cv2
import tp1


def Traitement(capture, Trf, u0, v0, vitesse_rotation, ratio_t, t_deb, t_fin):
    ret, trame = capture.read()
    if ret == True:
        X, Y, numcomp = trame.shape
        timestamps = [capture.get(cv2.CAP_PROP_POS_MSEC)]
        if timestamps[0] >= t_deb:
            timecode = timestamps[0]
            temps = ratio_t*(timecode-t_deb)/1000.0
            angle_rotation = 180.0*temps*vitesse_rotation/np.pi
            rotate_matrix = cv2.getRotationMatrix2D(
                center=(u0, v0), angle=angle_rotation, scale=1)
            height, width = trame.shape[:2]
            img1 = cv2.warpPerspective(trame, Trf, dsize=(width, height))
            img2 = cv2.warpAffine(
                src=img1, M=rotate_matrix, dsize=(width, height))
            cv2.imshow('sequence video', img2)
        if timestamps[0] > t_fin:
            return True
    k = cv2.waitKey(10)
    # on quitte en tapant la touche ESC
    if k % 256 == 27:
        return True
    else:
        return False


Trf = tp1.calctform()
t_deb = 8000
t_fin = 83000
u0 = 640
v0 = 360
vitesse_rotation = 7.292115e-5
ratio_t = 650
capture = cv2.VideoCapture('videosource.mp4')
if capture.isOpened() == False:
    print('source inexistante')
    capture.release()
    cv2.destroyAllWindows()
else:
    print('Touche ESC pour quitter')
    while Traitement(capture, Trf, u0, v0, vitesse_rotation, ratio_t, t_deb, t_fin) == False:
        pass
    capture.release()
    cv2.destroyAllWindows()
