import numpy as np
from scipy.optimize import least_squares
import cv2


def prisedevue(nomimage='imagetimelapse.jpg'):
    img = cv2.imread(nomimage)
    cv2.imshow('Image initiale', img)
    print('dimension de l\'image', img.shape[0:2])
    Trf = calctform()
    img2 = cv2.warpPerspective(img, Trf, img.shape[1::-1])
    cv2.imshow('Image transformee', img2)
    cv2.waitKey(0)


def calctform():
    Rot, alph, u0, v0 = calibration()
    # A MOFIFIER
    # Matrice intervenant dans le passage 2D-->3D
    Mat = np.array([[1/alph, 0,      -u0/alph],
                    [0,      1/alph, -v0/alph],
                    [0,      0,      1       ]])

    # A MOFIFIER
    # Nouveaux paramètres pour la reprojection
    alphp = 300
    u0p = u0
    v0p = v0

    PrjSor = np.array([[alphp, 0, u0p], [0, alphp, v0p], [0, 0, 1]])
    Trf = PrjSor@Rot.T@Mat
    return Trf


def calibration():
    # On suppose que le centre de l'image correspond bien à l'axe optique du
    # capteur.
    u0 = 640
    v0 = 360

    # les positions des étoiles dans l'image sont dans uv0
    # (format [colonne; ligne], stockées colonne par colonne)
    uv0 = np.array([[402, 145], [853, 15], [909, 389], [403, 303], [623, 574], [337, 599]]).T

    #  les directions des étoiles sont dans XYZ0
    #  (format [X0; Y0; Z0], stockées colonne par colonne)
    sind = lambda x : np.sin(np.deg2rad(x))
    cosd = lambda x : np.cos(np.deg2rad(x))

    # ascencion droite, déclinaison
    angles_deg = [[187.7903, -57.1146],
                  [ 95.9881, -52.6955],
                  [ 24.4292, -57.2371],
                  [219.8763, -60.8175],
                  [306.4124, -56.6688],
                  [264.3298, -42.9978]]
    XYZ0 = np.array([[cosd(alpha)*cosd(gamma), -sind(alpha)*cosd(gamma), -sind(gamma)] for alpha, gamma in angles_deg]).T
    print('uv0\n', uv0)
    print('XYZ0\n', XYZ0)

    param0 = np.array([0, 0, 0, 0])
    param = least_squares(lambda param: recherche(
        param, XYZ0, uv0, u0, v0), param0, ftol=1e-10, xtol=1e-10, gtol=1e-10)
    L = param.x[0]
    T = param.x[1]
    R = param.x[2]
    alph = param.x[3]
    print('L(en °),T(en °),R(en °),alpha(en pixels)')
    print(param.x)
    Rot = matrice_rotation(L, T, R)
    uv = projection_infini(XYZ0, u0, v0, alph, L, T, R)
    print('uv-uv0 (en pixels)')
    print(uv-uv0)
    print('écart type')
    print(np.std(uv-uv0, ddof=1))
    return Rot, alph, u0, v0


def recherche(param, XYZ0, uv0, u0, v0):
    # fonction appelée autant de fois que nécessaire
    # par la fonction least_squares
    # param est constitué des 3 angles L,T,R et alpha
    L = param[0]
    T = param[1]
    R = param[2]
    alph = param[3]
    uv = projection_infini(XYZ0, u0, v0, alph, L, T, R)
    return (uv-uv0).flatten()


def projection_infini(XYZ, u0, v0, alph, L, T, R):
    # calcul des projections
    # XYZ peut être un vecteur colonne 3x1 ou
    # une matrice 3xk pour traiter k points
    # uv sera alors un vecteur colonne 2x1 ou
    # une matrice 2xk
    Rot = matrice_rotation(L, T, R)
    Prj = np.array([[alph, 0, u0], [0, alph, v0], [0, 0, 1]])
    uv = Prj@Rot@XYZ
    uv = uv/uv[2, :]
    uv = uv[0:2, :]
    return uv


def matrice_rotation(lacet, tangage, roulis):
    # matrice "RTL" de rotation
    lacet = np.deg2rad(lacet)
    tangage = np.deg2rad(tangage)
    roulis = np.deg2rad(roulis)
    L = np.array([[np.cos(lacet), 0, -np.sin(lacet)],
                 [0, 1, 0], [np.sin(lacet), 0, np.cos(lacet)]])
    T = np.array([[1, 0, 0], [0, np.cos(tangage), np.sin(tangage)], [
                 0, -np.sin(tangage), np.cos(tangage)]])
    R = np.array([[np.cos(roulis), np.sin(roulis), 0],
                 [-np.sin(roulis), np.cos(roulis), 0], [0, 0, 1]])
    return R@T@L
