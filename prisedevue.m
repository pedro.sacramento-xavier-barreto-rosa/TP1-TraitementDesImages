% fonction appliquant à l'image d'entrée
% la transformation calculée
function prisedevue(nomimage)
if nargin == 0
    nomimage='imagetimelapse.jpg';
end
% lecture de l'image
RGB=imread(nomimage);
% calcul de la transformation
tform=calctform();
% application de la  transformation à l'image
RGBrecal = imwarp(RGB,tform,'OutputView',imref2d(size(RGB)));
% visualisation de l'image
figure,imshow(RGBrecal)